/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Facets
define(
	'Facets.ItemCell.View'
,	[
		'ProductLine.Stock.View'
	,	'Product.Model'
	,	'GlobalViews.StarRating.View'
	,	'Cart.QuickAddToCart.View'
	,	'ProductViews.Option.View'
	,	'ProductLine.StockDescription.View'
	,	'SC.Configuration'
	,	'Utils'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'underscore'
	]
,	function (
		ProductLineStockView
	,	ProductModel
	,	GlobalViewsStarRating
	,	CartQuickAddToCartView
	,	ProductViewsOptionView
	,	ProductLineStockDescriptionView
	,	Configuration
	,	Utils

	,	Backbone
	,	BackboneCompositeView
	,	BackboneCollectionView
	,	_
	)
{
	'use strict';
	var image_logic;

	// @class Facets.ItemCell.View @extends Backbone.View
	return Backbone.View.extend({

		//@method initialize Override default method to convert this View into Composite
		//@param {Facets.ItemCell.View.Initialize.Options} options
		//@return {Void}
		initialize: function ()
		{
			BackboneCompositeView.add(this);
			image_logic=true;
		}

	,	contextData: {
			'item': function ()
			{
				return Utils.deepCopy(this.model);
			}
		}

	,	childViews: {
			'ItemViews.Stock': function ()
			{
				return new ProductLineStockView({
					model: this.model
				});
			}

		,	'GlobalViews.StarRating': function()
			{

				var view = new GlobalViewsStarRating({
					model: this.model
				,	showRatingCount: false
				,	queryOptions: Utils.parseUrlOptions(location.href)
				});

				return this.options.showStarRating === false ? null : view;
			}

		,	'ItemDetails.Options': function()
			{
				var options_configuration = Configuration.get('ItemOptions.optionsConfiguration', []);

				return new BackboneCollectionView({
					collection: _.filter(this.model.get('options').sortBy('index'), function (option)
					{
						var option_configuration = _.findWhere(options_configuration, {cartOptionId: option.get('cartOptionId')});
						return option_configuration && option_configuration.showSelectorInList;
					})
				,	childView: ProductViewsOptionView
				,	viewsPerRow: 1
				,	childViewOptions: {
						item: this.model
					,	templateName: 'facetCell'
					,	showLink: true
					,	hideLabel: true
					,	showSmall: true
					}
				});
			}

		,	'Cart.QuickAddToCart': function ()
			{
				var product = new ProductModel({
					item: this.model
				,	quantity: this.model.get('_minimumQuantity', true)
				});

				return new CartQuickAddToCartView({
					model: product
				,	application: this.options.application
				});
			}

		,	'StockDescription': function ()
			{
				return new ProductLineStockDescriptionView({
					model: this.model
				});
			}
		}

		// @method getContext @returns {Facets.ItemCell.View.Context}
	,	getContext: function ()
		{
			var is_thumbnail=true;
			var image_logic=true;
			
             if(image_logic){
           if(jQuery(".facets-facets-display-filter[data-id='colorfamily']").length>0&&jQuery(".facets-facets-display-filter[data-facet-id='Clearance-Center']").length==0 && this.model.get('_matrixChilds').length!=0)  
             {              
                           image_logic=false;
             	           is_thumbnail=false;
				            var matrix_child_data=this.model.get('_matrixChilds').toJSON();
				            var Image_data=this.model.get('_images');
							var options=this.model.getPosibleOptions();
				            var color_name;
				            var color_id;
				            var Image_url;
				            var color_options=options[2].values;
             	            var facet_string =  jQuery(".facets-facets-display-filter[data-id='colorfamily']").attr("data-facet-id");
				             facet_string=facet_string.replace("-SLASH-", "/");
				             facet_string=facet_string.replace("~", "-");
				             //console.log(facet_string);
				              jQuery.each(matrix_child_data,function(index,value)
				             {
				                
				                var colorfamily=value.custitem_web_facet_color;   

				                if(jQuery(".facets-facets-display-filter[data-id='clearance']").length>0)
				               { is_thumbnail=true;
				            }
				            if(jQuery(".facets-facets-display-filter[data-id='clearance']").length==0)
				            {
				            	if(colorfamily.indexOf(facet_string)!=-1)
				                {
				                	color_name=value.custitem_matrix_color;
				                	

				                }
				            }
				             if(typeof(color_name)!="undefined")
						          {
						          	return false;
						          }
				            
				             });

				               jQuery.each(color_options,function(index,value)
				             {

                                if(index>0)
                                {
                                	if(value.label==color_name)
                                	{
                                		color_id=value.internalid;
                                	}
                                }
                                if(typeof(color_id)!="undefined")
						          {
						          	return false;
						          }

				             });



				     
	
							 	var url_string=this.model.get('_sku')+"."+color_id;
							    
							 	jQuery.each(Image_data,function(index,Image_data)
							  {
					           
					           
					           if (Image_data.url.indexOf(url_string) >0)
					           {
					              Image_url=Image_data.url;
					              

					              

					           }
					           if(typeof(Image_url)!="undefined")
					          {
					          	return false;
					          }
							  });        


					             if(typeof(Image_url)=="undefined")
					          {
					          	is_thumbnail=true;
					          }
					                   
             }

//Start of logic for Default options selection URL appending by retrieving the color available from ItemOption View.

         	var itId=this.model.get('_id');
         	var facetSku=this.model.get('_sku');
			var coloriD='';
			var updatedUrl='';
			var linkUrl=this.model.get('_url');
			var optionUrl='';
           var size;
            var colorValue;
            var colorLabel=[];
            var availableColor='';
            var sizeValue='';
            if(this.model.get('_optionsDetails').matrixtype=="parent")
            {    var arr = this.model.get('_optionsDetails').fields; 		
                 		if(arr){
             				for (var i = 0; i < arr.length; ++i) {
                				
                 				if(arr[i].label === "Size")
					                 {
					                 	
					                 	sizeValue = arr[i].values[1].internalid;
					                 	size=arr[i].values[1].label;
					                 	
					                 }
					             }
					         }
					         var matrix_child_data=this.model.get("_matrixChilds").toJSON();
					         var color_avail=new Array();
            jQuery.each(matrix_child_data,function(index,value)
         {
                
               if(size==value.custitem_matrix_size)
                {
                   color_avail[color_avail.length]=value.custitem_matrix_color;
                   
                   availableColor=color_avail[0];
                }
               
         });
               
					          if(arr){
             				for (var i = 0; i < arr.length; ++i) {
             					
					            if(arr[i].label === "Color")
					                 {
					                 	
					                 	var a=arr[i].values;
					                 	jQuery.each(a,function(index,value){
					                 	         
								                 if(jQuery.inArray(value.label, color_avail) != -1)
								                 {
								                 	colorValue=value.internalid;
								                 	
								                 }
								                if(typeof(colorValue)!="undefined")
								                {
								                	return false;
								                }
								                 	
					                 	
					                 });
					                
					                 }
             				}
             			}	
             			 
             			if(sizeValue)
             			{
             			
             				
            			 optionUrl=linkUrl+"?size="+sizeValue+"&color="+colorValue;
            							//console.log("optionUrl "+optionUrl);	
           				}

           	} // End of condition for matrix parent

//End of logic for Default options selection

         }

			 var displayname = this.model.get('_storedisplayname');
         if(displayname)
			   {		
					if(displayname.indexOf(':') != -1){
					   
		             var newchar = '<br/>';
		              displayname = displayname.split(':').join(newchar);
		             //series=series.replaceAll(" ","_");
		            }
		        }		  


			//@class Facets.ItemCell.View.Context
			return {
				// @property {String} itemId
				itemId: this.model.get('_id')
				// @property {String} name
			,	name: this.model.get('_name')
				// @property {String} url
			,	url: this.model.get('_url')
				//@property {String} sku
			,	sku: this.model.getSku()
				// @property {Boolean} isEnvironmentBrowser
			,	isEnvironmentBrowser: SC.ENVIRONMENT.jsEnvironment === 'browser' && !SC.ENVIRONMENT.isTouchEnabled
				// @property {ImageContainer} thumbnail
			,	thumbnail: this.model.getThumbnail()
				// @property {Boolean} itemIsNavigable
			,	itemIsNavigable: !_.isUndefined(this.options.itemIsNavigable) ? !!this.options.itemIsNavigable : true
				//@property {Boolean} showRating
			,	showRating: SC.ENVIRONMENT.REVIEWS_CONFIG && SC.ENVIRONMENT.REVIEWS_CONFIG.enabled
				// @property {Number} rating
			,	rating: this.model.get('_rating')
				//@property {String} track_productlist_list
			,	track_productlist_list: this.model.get('track_productlist_list')
				//@property {String} track_productlist_position
			,	track_productlist_position: this.model.get('track_productlist_position')
				//@property {String} track_productlist_category
			,	track_productlist_category: this.model.get('track_productlist_category')
			,   displayname:displayname
			,   webSeriesSubtitle:this.model.get('_webSeriesSubtitle')
			,   isClearance:this.model.get('_webSeries')==='Clearance Center'
			,   isQuickShip:this.model.get('_webSeries')==='QuickShip'

			};
			//@class Facets.ItemCell.View
		}
	});
});


//@class Facets.ItemCell.View.Initialize.Options
//@property {Item.Model} model
//@property {ApplicationSkeleton} application
//@property {Boolean?} itemIsNavigable
